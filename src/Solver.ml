(*
   As explained in the README.md ("Abstracting over an effect"),
   this module as well as other modules is parametrized over
   an arbitrary effect [T : Functor].
*)

module Make (T : Utils.Functor) = struct
  module Constraint = Constraint.Make(T)
  module SatConstraint = SatConstraint.Make(T)
  module ConstraintSimplifier = ConstraintSimplifier.Make(T)
  module ConstraintPrinter = ConstraintPrinter.Make(T)

  type env = Unif.Env.t
  type log = PPrint.document list

  let make_logger c0 =
    let logs = Queue.create () in
    let c0_erased = SatConstraint.erase c0 in
    let add_to_log env =
      let doc =
        c0_erased
        |> ConstraintSimplifier.simplify env
        |> ConstraintPrinter.print_sat_constraint
      in
      Queue.add doc logs
    in
    let get_log () =
      logs |> Queue.to_seq |> List.of_seq
    in
    add_to_log, get_log

  (** See [../README.md] ("High-level description") or [Solver.mli]
      for a description of normal constraints and
      our expectations regarding the [eval] function. *)
  type ('a, 'e) normal_constraint =
    | NRet of 'a Constraint.on_sol
    | NErr of 'e
    | NDo of ('a, 'e) Constraint.t T.t

  type ('a, 'e) contrainte_equilibree =
    (* Comme normal_constraint, sauf que la profondeur de remontée
    du Do est indiquée, pour permettre une gestion équilibrée du cas
    de conj (cf la partie de solver qui traite le cas Conj) *)
    | ERet of 'a Constraint.on_sol
    | EErr of 'e
    | EDo of int * ('a, 'e) Constraint.t T.t

  let to_nc = function
    | ERet x -> NRet x
    | EErr x -> NErr x
    | EDo (_, x) -> NDo x


  let conversion_erreur (env:env) : Unif.err -> Constraint.eq_error = 
    (*Convertit une Unif.err en Constraint.eq_error*)
    let dec = Decode.decode env in function
    | Unif.Clash (v1, v2) -> Constraint.Clash (dec v1, dec v2)
    | Unif.Cycle c -> Constraint.Cycle c


  let eval (type a e) ~log (env : env) (c0 : (a, e) Constraint.t)
    : log * env * (a, e) normal_constraint
  =
    let add_to_log, get_log =
      if log then make_logger c0
      else ignore, (fun _ -> [])
    in
    let maj_log : env -> unit =
      (* Met à jour le log s’il y a eu une modification de l’environnement
      depuis le dernier appel *)
      (*L’idée de cette astuce est due à Neven*)
      let ancien_env = ref None in
      (fun nouvel_env ->
        if (!ancien_env <> Some nouvel_env) then
          let () = ancien_env := Some nouvel_env in
          add_to_log nouvel_env)
    in
    let rec receval : type a e. env -> (a, e) Constraint.t -> 
      env * (a, e) contrainte_equilibree
      = fun env c0 -> begin
      let () = maj_log env in 
      match c0 with
      | Constraint.Ret on_sol -> (env, ERet on_sol)
      | Constraint.Err e -> (env, EErr e)
      | Constraint.Map (c1, f) -> 
        begin 
          let (nouvel_env, c_res) = receval env c1 in
          let res =
          match c_res with
          | ERet on_sol -> ERet (fun sol -> on_sol sol |> f)
          | EErr e -> EErr e
          | EDo (prof, constr) -> 
              let constr_map =
                constr |> T.map (fun c -> Constraint.Map (c, f)) in
              EDo (prof + 1, constr_map)
          in (nouvel_env, res)
        end
      | Constraint.MapErr (c1, f) -> 
        begin 
          let (nouvel_env, c_res) = receval env c1 in
          let res =
          match c_res with
          | ERet on_sol -> ERet on_sol
          | EErr e -> EErr (f e)
          | EDo (prof, constr) -> 
              let constr_map =
                constr |> T.map (fun c -> Constraint.MapErr (c, f) ) in
              EDo (prof + 1, constr_map)
          in (nouvel_env, res)
        end

      | Constraint.Conj (c1, c2) ->
        begin 
          let env1, retour1 = receval env c1 in
          let env2, retour2 = receval env1 c2 in
          match retour1, retour2 with
          | EErr e, _  | _, EErr e -> env2, EErr e
          (* dans les deux cas suivants, on remonte en premier
          le EDo qui vient du moins loin possible. C’est parfaitement inutile. *)
          | EDo (prof1, constr1), EDo (prof2, constr2) 
            when prof1 < prof2 -> 
              let constr =
                constr1 |> T.map (fun c -> 
                Constraint.Conj (c, Do (prof2, constr2))) in
              env2, EDo(prof1 + 1, constr)
          
          | EDo (prof1, constr1), EDo (prof2, constr2) 
            -> 
            let constr =
              constr2 |> T.map (fun c -> 
                Constraint.Conj (Do (prof1, constr1), c)) in
              env2, EDo(prof2 + 1, constr)
          | EDo (prof, constr), ERet on_sol -> 
              let c_resolu = Constraint.Ret on_sol in
              let constr_conj =
              constr |> T.map (fun c -> Constraint.Conj (c, c_resolu)) in
              env2, EDo (prof + 1, constr_conj)
          | ERet on_sol, EDo (prof, constr) ->
              let c_resolu = Constraint.Ret on_sol in
              let constr_conj =
              constr |> T.map (fun c -> Constraint.Conj (c_resolu, c)) in
              env2, EDo (prof + 1, constr_conj)
          | ERet on_sol1, ERet on_sol2 ->
              env2, ERet (fun sol -> (on_sol1 sol, on_sol2 sol))
        end


      | Constraint.Eq (w1, w2) ->
          Unif.unify env w1 w2
          |> Result.fold
            ~ok:(fun nouvel_env -> (nouvel_env, ERet (fun _ -> ())))
            ~error: (fun e -> (env, EErr (conversion_erreur env e)))
      | Constraint.Exist (v, o, constr) ->
        let nouvel_env = Unif.Env.add v o env in
        receval nouvel_env constr
      | Constraint.Decode v ->
          env, ERet (fun sol -> sol v) 
          

      | Constraint.Do (prof, constrT) -> env, EDo (prof, constrT)
      end
    in
    let (env, final_constr) = receval env c0 in
    (get_log (), env, final_constr |> to_nc)

end
