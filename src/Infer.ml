(** Infer contains the logic to generate an inference constraint from
    an untyped term, that will elaborate to an explicitly-typed term
    or fail with a type error. *)

(* You have to implement the [has_type] function below,
   which is the constraint-generation function. *)

module Make(T : Utils.Functor) = struct
  module Constraint = Constraint.Make(T)
  open Constraint
  module Untyped = Untyped.Make(T)

  (** The "environment" of the constraint generator maps each program
      variable to an inference variable representing its (monomorphic)
      type.

      For example, to infer the type of the term [lambda x. t], we
      will eventually call [has_type env t] with an environment
      mapping [x] to a local inference variable representing its type.
  *)
  module Env = Untyped.Var.Map
  type env = variable Env.t

  type err = eq_error =
    | Clash of STLC.ty Utils.clash
    | Cycle of Constraint.variable Utils.cycle

  type 'a constraint_ = ('a, err) Constraint.t

  let eq v1 v2 = Eq(v1, v2)
  let decode v = MapErr(Decode v, fun e -> Cycle e)
  let exist w o t = 
    (* helper function to simplify the chaining of Exist *)
    Exist (w, o, t)

  let assume_pair = function
    | [v1; v2] -> (v1, v2)
    | other ->
      Printf.ksprintf failwith
        "Error: this implementation currently only supports pairs,
         not tuples of size %d."
        (List.length other)

  (** This is a helper function to implement constraint generation for
      the [Annot] construct.
     
      [bind ty k] takes a type [ty], and a constraint [k] parametrized
      over a constraint variable. It creates a constraint context that
      binds a new constraint variable [?w] that must be equal to [ty],
      and places [k ?w] within this context.
      
      For example, if [ty] is the type [?v1 -> (?v2 -> ?v3)] , then
      [bind ty k] could be the constraint
        [∃(?w1 = ?v2 -> ?v3). ∃(?w2 = ?v1 -> ?w1). k ?w2], or equivalently
        [∃?w3 ?w4. ?w3 = ?v1 -> ?w4 ∧ ?w4 = ?v2 -> ?v3 ∧ k ?w3].
  *)
  let rec bind (ty : STLC.ty) 
  (k : Constraint.variable -> ('a, 'e) t) : ('a, 'e) t =
    let Constr ty_str = ty in
    match ty_str with
      | Var type_final -> let w = Var.fresh "a" in 
        exist w (Some (Var type_final)) (k w)
      | Arrow (t_arg, t_renv) -> 
          bind t_arg (fun w_arg ->
            bind t_renv (fun w_renv ->
              let w_fun = Var.fresh "f" in
              k w_fun
              |> exist w_fun (Some (Structure.Arrow (w_arg, w_renv)))
            ))
      | Prod l -> let (t1, t2) = assume_pair l in
          bind t1 (fun w1 ->
            bind t2 (fun w2 ->
              let w_couple = Var.fresh "c" in
              k w_couple
              |> exist w_couple (Some (Structure.Prod [w1; w2]))))

  (** This function generates a typing constraint from an untyped term:
      [has_type env t w] generates a constraint [C] which contains [w] as
      a free inference variable, such that [C] has a solution if and only
      if [t] is well-typed in [env], and in that case [w] is the type of [t].

      For example, if [t] is the term [lambda x. x], then [has_type env t w]
      generates a constraint equivalent to [∃?v. ?w = (?v -> ?v)].

      Precondition: when calling [has_type env t], [env] must map each
      term variable that is free in [t] to an inference variable.
  *)
  let rec has_type (env : env) (t : Untyped.term) 
                   (w : variable) : (STLC.term, err) t =
    match t with
    | Untyped.Var x ->
        let type_inference_x = Env.find x env in
        let+ _type_x = decode type_inference_x
        and+ () = eq type_inference_x w in
        STLC.Var x
    | Untyped.App (t, u) ->
        let wt = Var.fresh "f" in
        let wu = Var.fresh "x" in
        let terme_type =
          let+ terme_t = has_type env t wt
          and+ terme_u = has_type env u wu
          in STLC.App (terme_t, terme_u)
        in
        Exist (wt, Some (Structure.Arrow (wu,  w)), terme_type)
        |> exist wu None
    | Untyped.Abs (x, t) ->
        let wt = Var.fresh "s" in
        let wx = Var.fresh "x" in
        let wabs = Var.fresh "f" in
        begin
        let+ terme_t = 
          let env_local = Env.add x wx env in
          has_type env_local t wt
        and+ type_x = Ret (fun sol -> sol wx)
        and+ () = eq w wabs in
        STLC.Abs (x, type_x, terme_t)
        end
        |> exist wabs (Some (Structure.Arrow (wx, wt)))
        |> exist wt None
        |> exist wx None
    | Untyped.Let (x, t, u) ->
        let wx = Var.fresh "x" in
        begin
        let+ terme_t = has_type env t wx
        and+ terme_u = 
          let env_local = Env.add x wx env in
          has_type env_local u w
        and+ type_x = decode wx in
        STLC.Let (x, type_x, terme_t, terme_u)
        end
        |> exist wx None
    | Untyped.Annot (t, ty) ->
        bind ty (fun w2 ->
          let+ terme_t = has_type env t w2
          and+ () = eq w w2 in
          STLC.Annot (terme_t, ty))
    | Untyped.Tuple ts ->
      let (t1, t2) = assume_pair ts in
      let w1 = Var.fresh "a" in
      let w2 = Var.fresh "b" in
      let w_pair = Var.fresh "w" in
      begin
      let+ terme_t1 = has_type env t1 w1
      and+ terme_t2 = has_type env t2 w2
      and+ () = eq w w_pair in 
      STLC.Tuple [terme_t1; terme_t2]
      end
      |> exist w_pair (Some (Structure.Prod [w1; w2]))
      |> exist w2 None
      |> exist w1 None
    | Untyped.LetTuple (xs, t, u) ->
      let (x1, x2) = assume_pair xs in
      let wx1 = Var.fresh "x1" in
      let wx2 = Var.fresh "x2" in
      let w_couple = Var.fresh "pair" in
      begin
        let+ terme_t = has_type env t w_couple 
        and+ terme_u =
          let env_local = 
            Env.add x1 wx1 env
            |> Env.add x2 wx2 in
          has_type env_local u w
        and+ type_x1 = decode wx1
        and+ type_x2 = decode wx2
        in
        STLC.LetTuple ([x1, type_x1; x2, type_x2], terme_t, terme_u)
      end
      |> exist w_couple (Some (Structure.Prod [wx1; wx2]))
      |> exist wx2 None
      |> exist wx1 None
    | Untyped.Do p ->
      let constr_t =
        p
        |> T.map (fun t -> has_type env t w)
      in
      Do (0, constr_t)
end
