module Make(M : Utils.MonadPlus) = struct
  module Untyped = Untyped.Make(M)
  module Constraint = Constraint.Make(M)
  module Infer = Infer.Make(M)
  module Solver = Solver.Make(M)

  (* just in case... *)
  module TeVarSet = Untyped.Var.Set
  module TyVarSet = STLC.TyVar.Set


  type spec_gen =
    { initial_lambda : bool; (* vaut vrai si le terme généré _doit_ être une
    λ-abstraction *)
      novar : bool; (* vaut vrai si le terme généré _ne doit pas_ être une
      variable *)
      env : Untyped.Var.t list; (* liste des variables définies *)
    } 



let untyped : Untyped.term =
  let rec gen (spec : spec_gen) : Untyped.term =
    let fresh_var = Untyped.Var.namegen [|"x"; "y"; "z"; "u"; "v"; "w"; "t";|] in
    let (let*) x f = M.bind x f in
    let open Untyped in
    let cacher x = M.return (Do x) in
    (*Utilisé pour générer moins de λ-abstractions, et plus de termes
      intéressants *)
    Do (M.delay @@ fun () ->
      if spec.initial_lambda then 
        begin
        let x = fresh_var () in
        let* continue_lambda = M.one_of [|true; false|] in
        M.return (Abs (x, gen
        { initial_lambda = continue_lambda;
          novar = false;
          env = x::spec.env }))
        |> cacher
        end
      else
        M.sum [
        begin 
            M.return (
              App( gen {spec with novar = false},
                   gen {spec with novar = false}))
        end ;
        begin
          let x = fresh_var () in
          let* do_lambdas = M.one_of [|true; false|] in
          M.return (
            Let (x, 
            gen {spec with initial_lambda = do_lambdas; novar = true},
            gen {env=(x::spec.env); initial_lambda = false; novar = true}))
        end;
        begin
          if spec.novar then M.fail else
          spec.env
          |> List.map (fun x -> (Var x))
          |> Array.of_list
          |> M.one_of 
        end ;
        begin
          M.return (
            Tuple [
              gen {spec with novar = false};
              gen {spec with novar = false}
            ]
          )
        end;
        ]
    )
    in gen {env = []; initial_lambda = true; novar = false}

let constraint_ : (STLC.term, Infer.err) Constraint.t =
  let w = Constraint.Var.fresh "final_type" in
  Constraint.(Exist (w, None,
    Infer.has_type
      Untyped.Var.Map.empty
      untyped
      w))

let typed ~depth =
  let rec rectyped depth env constr =
    let _logs, nouvel_env, norm_constr =
      Solver.eval ~log:false env constr in
    match norm_constr with
      | NRet v -> 
          if true then begin
          if depth = 0 then 
            let () = Decode.rafraichir () in
            let () = STLC.TeVar.rafraichir () in
            let () = STLC.TyVar.rafraichir () in
            M.return (v (Decode.decode nouvel_env))
          else M.fail
          end else 
            let () = Decode.rafraichir () in
            let () = STLC.TeVar.rafraichir () in
            let () = STLC.TyVar.rafraichir () in
            M.return (v (Decode.decode nouvel_env))
        (* Les deux branches donnent un générateur qui
          fonctionne, mais le générateur aléatoire donne
          des termes plus intéressants dans l’état actuel. *)
            
      | NErr _e -> M.fail
      | NDo c_t -> if depth = 0 then M.fail else
          M.bind c_t (rectyped (depth-1) nouvel_env)
  in
  rectyped (2 * depth -1)  Unif.Env.empty constraint_
  (* On utilise une profondeur plus grande que prévu pour compenser le fait
  qu’on ai caché les λ-abstractions *)


  (* This definition uses [constraint_] to generate well-typed terms.
     An informal description of a possible way to do this is described
     in the README, Section "Two or three effect instances", where
     the function is valled [gen]:

     > it is possible to define a function
     >
     >     val gen : depth:int -> ('a, 'e) constraint -> ('a, 'e) result M.t
     >
     > on top of `eval`, that returns all the results that can be reached by
     > expanding `Do` nodes using `M.bind`, recursively, exactly `depth`
     > times. (Another natural choice would be to generate all the terms that
     > can be reached by expanding `Do` nodes *at most* `depth` times, but
     > this typically gives a worse generator.)
  *)

end
