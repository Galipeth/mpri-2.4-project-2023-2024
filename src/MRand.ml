(* On représente la structure par un arbre, dont les feuilles sont des résultats *)

type 'a t =
  | Nil
  | Leaf of 'a
  | Choice of 'a t array
  | Delay of 'a memo ref
  (* On utilise ici une référence pour mémoïser le calcul de la fonction qui
     était délayée *)

and 'a memo =
  | Not_comp of (unit -> 'a t)
  | Done of 'a t


let rec map (f : 'a -> 'b) (s : 'a t) : 'b t =
  match s with
  | Nil -> Nil
  | Leaf a -> Leaf (f a)
  | Choice arr -> Choice (Array.map (map f) arr)
  | Delay d -> 
      begin 
        match !d with 
        | Not_comp comp -> 
            Delay (ref (Not_comp (fun () -> map f (comp ()))))
        | Done s -> Delay(ref (Done (map f s)))
      end

let inline : 'a t -> 'a t = function s ->
  (* Sert à ramener tout l’arbre dans un seul nœud de la forme
     Choice[Leaf | Delay Not Comp], pour donner une meilleure distribution
     aléatoire. Cette solution n’est pas parfaite, puisqu’au fur et à mesure
     qu’on l’appelle, on diminue la probabilité de parcourir un chemin qu’on n’a
     pas encore calculé*)
  let rec parcours acc = function
    | Nil -> acc
    | Leaf _ as l-> l::acc
    | Choice arr -> Array.fold_left parcours acc arr
    | Delay d -> 
      begin 
        match !d with 
        | Not_comp _ ->  Delay d :: acc
        | Done s -> parcours acc s
      end
  in
  let possibilites = parcours [] s in
  Choice (Array.of_list possibilites)



let a_ete_modifie = ref false
  (* référence globale, pour réactiver au besoin la fonction inline *)


let return (x : 'a) : 'a t =
  Leaf x

let sum (li : 'a t list) : 'a t =
  Choice (Array.of_list li)

let rec bind (sa : 'a t) (f : 'a -> 'b t) : 'b t =
  match sa with
  | Nil -> Nil
  | Leaf a -> f a
  | Choice arr -> 
      Array.map (fun a -> bind a f) arr
      |> Array.to_list
      |> sum
  | Delay d -> 
      begin 
        match !d with 
        | Not_comp comp -> 
            Delay (ref (Not_comp (fun () -> bind (comp ()) f)))
        | Done s -> Delay(ref (Done (bind s f)))
      end

let delay (f : unit -> 'a t) : 'a t =
  Delay (ref (Not_comp f))

  

let fail : 'a t =
  Nil

let one_of (vs : 'a array) : 'a t =
  Choice (Array.map (fun a -> return a) vs)


let rec get_one : 'a t -> 'a option = function
  | Nil -> None
  | Leaf a -> Some a
  | Choice arr ->
    let n = Array.length arr in
    if n = 0 then None
    else
    let i = Random.int n in
    get_one arr.(i)
  | Delay d -> 
      begin 
        match !d with 
        | Not_comp comp -> 
            let resultat_calcule = 
              comp ()
              |> inline in
            let () = a_ete_modifie := true in
            let () = d := Done resultat_calcule in
            get_one resultat_calcule
        | Done s -> get_one s
      end

      


let run (s : 'a t) : 'a Seq.t =
  let get =
    let rs = ref s in
    (fun () ->
    let () = 
      if !a_ete_modifie then
        let () = rs := inline !rs in
        a_ete_modifie := false
        (* on réapplique inline à chaque fois qu’un calcul est fait *)
    in
    get_one !rs)
  in
  Seq.forever get
  |> Seq.filter (function Some _ -> true | None -> false)
  (* Attention, cette fonction n’a aucune garantie de terminer *)
  |> Seq.map (function Some a -> a | None -> assert false)
  
