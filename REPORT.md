# Typeur et générateur de λ-termes

Une version pdf de ce rapport est disponible, mise en page par mes soins de
rédacteur du journal de l’ens. Pour des raisons d’accessibilité, son contenu est
aussi disponible dans ce fichier, mais je recommande plutôt la lecture de
`rapport.pdf`

##Vue d’ensemble du projet

L’objectif du projet est de construire un typeur pour le λ-calcul
simplement typé, construit de telle sorte à pouvoir être réutilisé dans un
énumérateur des λ-termes bien typés.

J’ai implémenté la partie obligatoire du projet, et je me suis ensuite concentré
sur la génération de jolis termes.

##Générer des jolis termes

Une fois que mon générateur pour `MSeq`
fonctionnait j’ai, après mon tour de la coloc en chantant,
constaté que les termes générés n’étaient pas très intéressants~:
l’identité `id`, 
c’est un joli terme, mais `id id`, `id (id id)`,
`(id id) id` ne m’intéressent pas trop.

Je me suis défini une notion de terme *satisfaisant*, et j’ai restreint
les termes générés aux termes satisfaisants~: 

Un β-redex n’est pas satisfaisant, le générateur ne produit donc que des
λ-abstractions, et pas de termes de la forme `App (Abs, _)`.

Par ailleurs, les `let x = y in x` sont assez peu satisfaisants, donc
j’ai interdit au générateur de générer des variables seules dans les branches
d’un `let`.

La grammaire des termes satisfaisants générés est explicitée en figure dans le
pdf, et elle permet d’atteindre, à β-réduction près, toutes les valeurs.

Une amélioration que j’aurais voulu implémenter~: considérer uniqumement les
λ-termes dans lesquels les variables définies par un let in sont
utilisées.

##Cacher certains termes au générateur~?

Dans l’optique de rendre le générateur exhaustif plus agréable,
j’ai choisi de cacher les λ-abstraction derrière deux constructeurs
`Do`. De la sorte, à profondeur fixée, le générateur produit
plus de termes utilisant les autres constructeurs (qui en général vont
impliquer deux constructeurs `Do`) également.

La conséquence est que la profondeur de recherche est multipliée par deux,
pour contrebalancer sur les petites valeurs. 
En contrepartie, le générateur aléatoire va commencer à prendre
du temps pour des valeurs de profondeurs plus faibles (10 sur mon ordinateur).

## Implémentation de MRand

Lorsque j’ai implémenté pour la première fois `MRand`,
je n’avais pas très bien compris ce qu’il fallait faire. Finalement, après
plusieurs itéartions, j’ai quelque chose d’assez satisfaisant.

Le code est commenté pour présenter les choix qui ont été faits.

## Les contraintes équilibrées

Dimanche, 22h. Une idée de génie passe dans ma tête~:
"Dans `Solver`, quand on doit remonter un `NDo` dans une
`Constraint.Conj`, je choisis toujours de remonter celui de gauche en
premier, ça ressemble à un parcours en profondeur, est-ce que je n’aurai pas des
termes plus intéressants avec un parcours en largeur~?"

Je pensais que ce serait une amélioration notable facile à implémenter avant
d’aller me coucher. Je me suis
trompé deux fois.

Le résultat est un nouveau type de contrainte dite équilibrée, dont la seule
différence est que les `Do` indiquent aussi la profondeur de laquelle
ils ont été remonté dans l’arbre de la contrainte, et un choix lors du
traitement de `Conj` dans `Solver`, pour faire remonter en premier
le `Do` qui vient du moins loin en premier.

Ce n’était pas agréable à implémenter, car, dans le traitement de 
`Constraint.Conj (a, b)`les deux types de `a` et `b`
ne sont pas interchangeables. Cela donne place à une duplication désagréable du
code dans `Solver.ml`

Le lendemain matin, à la lumière d’une bonne nuit de sommeil, je me suis
aperçu que cette modification était *carrément inutile* !
En effet, `generator` ne traite uniquement les contraintes qui
contiennent exactement `depth` nœuds `Do`, indépendamment de
l’ordre dans lequel ceux-ci sont dépliés…

La morale de cette histoire~: quand on a une "bonne idée", 
mais qu’il est bientôt l’heure de dormir, on la note sur un papier et on la
reregarde le lendemain matin.


## Le problème des tables globales

Lorsque le générateur fonctionnait enfin, quelques tests
ont montrés qu’il hallucinait complètement les annotations de types.

Par exemple, le terme 
`lambda (x : α). lambda (x/1 : α/2). x (x (x x/1))`, généré à ce moment,
est tout à fait
typable, mais les annotations de types sont fausses.

Cela était dû à la table de hachage de `Decode`, qui était globale, et
non réinitialisée entre chaque terme. 

J’ai donc rajouté une fonction `rafraichir` aux modules qui utilisaient
des tables des hachages globales. Cela a pour conséquence que les annotations
des termes générés sont corrects, mais aussi que les termes générés sont plus
beaux~: les variables de types ne sont plus numérotés avec des
numéros à rallonges.














