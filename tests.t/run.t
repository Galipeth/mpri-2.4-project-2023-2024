# TL;DR

To run the tests, run

```
dune runtest
```

from the root of the project directory. If this outputs
nothing, the testsuite passes. If this outputs a diff, it
means that there is a mismatch between the recorded/reference
output and the behavior of your program.

To *promote* the tests outputs (that is, to modify the reference
output to match the current behavior of your program), run

```
dune runtest
dune promote
```

When you submit your project, please check that `dune runtest` does
not produce a diff -- the recorded output should match your
program. If some outputs are wrong / not what you would expect, please
explain this in the present file.


# Intro

This file is a "dune cram test" as explained at
> https://dune.readthedocs.io/en/stable/tests.html#cram-tests

The idea is to write 2-indented command lines prefixed by
a dollar sign. The tool will run the command and check that
the output corresponds to the output recorded in the file.

  $ echo example
  example

To run the tests, just run `dune runtest` at the root of the
project. This will show you a diff between the observed
output and the recorded output of the test -- we consider
that the test 'passes' if the diff is empty.  
In particular, if you run `dune runtest` and you see no
output, this is good! It means there was no change in the
test output.

If you think that the new output is better than the previous
output, run `dune promote`; dune will rewrite the recorded
outputs to match the observed outputs. (You can also modify
outputs by hand but this is more cumbersome.)

It is totally okay to have some test outputs recorded in
your repository that are known to be broken -- because there
is a bug, or some feature is not documented yet. Feel free
to use the free-form comments in run.t to mention explicitly
that the output is broken. (But then please remember, as the
output changes in the future, to also update your comments.)


# The tests

The tests below use the `minihell` program defined in
../bin/minihell.ml, called on the *.test files stored in the
present directory. If you want to add new tests, just add
new test files and then new commands below to exercise them.

`minihell` takes untyped programs as input and will
type-check and elaborate them. It can show many things
depending on the input flags passed. By default we ask
`minihell` to repeat the source file (to make the recorded
output here more pleasant to read) and to show the generated
constraint. It will also show the result type and the
elaborated term.

  $ FLAGS="--show-source --show-constraint"

Remark: You can call minihell from the command-line yourself
by using either
> dune exec bin/minihell.exe -- <arguments>
or
> dune exec minihell -- <arguments>
(The latter short form, used in the tests below, is available thanks
to the bin/dune content.)


## Simple tests

`id_poly` is just the polymorphic identity.

  $ minihell $FLAGS id_poly.test
  Input term:
    lambda x. x
  
  Generated constraint:
    ∃?final_type.
      (∃?x ?s (?f = ?x -> ?s). decode ?x ∧ ?x = ?s ∧ ?final_type = ?f)
      ∧ decode ?final_type
  
  Inferred type:
    α -> α
  
  Elaborated term:
    lambda (x : α). x
  

`id_int` is the monomorphic identity on the type `int`. Note
that we have not implemented support for a built-in `int`
type, this is just an abstract/rigid type variable: `Constr
(Var ...)` at type `STLC.ty`.

  $ minihell $FLAGS id_int.test
  Input term:
    lambda x. (x : int)
  
  Generated constraint:
    ∃?final_type.
      (∃?x ?s (?f = ?x -> ?s).
        (∃(?a = int). decode ?x ∧ ?x = ?a ∧ ?s = ?a) ∧ ?final_type = ?f)
      ∧ decode ?final_type
  
  Inferred type:
    int -> int
  
  Elaborated term:
    lambda (x : int). (x : int)
  

## Logging the constraint-solving process

You can ask `minihell` to show how the constraint evolves as
the solver progresses and accumulates more information on
the inference variables.

  $ minihell $FLAGS --log-solver id_int.test
  Input term:
    lambda x. (x : int)
  
  Generated constraint:
    ∃?final_type.
      (∃?x ?s (?f = ?x -> ?s).
        (∃(?a = int). decode ?x ∧ ?x = ?a ∧ ?s = ?a) ∧ ?final_type = ?f)
      ∧ decode ?final_type
  
  Constraint solving log:
    ∃?final_type.
      decode ?final_type
      ∧ (∃?x ?s (?f = ?x -> ?s).
        ?final_type = ?f ∧ (∃(?a = int). ?s = ?a ∧ ?x = ?a ∧ decode ?x))
    ∃?final_type.
      decode ?final_type
      ∧ (∃?x ?s (?f = ?x -> ?s).
        ?final_type = ?f ∧ (∃(?a = int). ?s = ?a ∧ ?x = ?a ∧ decode ?x))
    ∃?x ?final_type.
      decode ?final_type
      ∧ (∃?s (?f = ?x -> ?s).
        ?final_type = ?f ∧ (∃(?a = int). ?s = ?a ∧ ?x = ?a ∧ decode ?x))
    ∃?x ?s ?final_type.
      decode ?final_type
      ∧ (∃(?f = ?x -> ?s).
        ?final_type = ?f ∧ (∃(?a = int). ?s = ?a ∧ ?x = ?a ∧ decode ?x))
    ∃?x ?s ?final_type (?f = ?x -> ?s).
      decode ?final_type
      ∧ ?final_type = ?f
      ∧ (∃(?a = int). ?s = ?a ∧ ?x = ?a ∧ decode ?x)
    ∃?x ?s ?final_type (?f = ?x -> ?s) (?a = int).
      decode ?final_type ∧ ?final_type = ?f ∧ ?s = ?a ∧ ?x = ?a ∧ decode ?x
    ∃(?x = int) ?s ?final_type (?f = ?x -> ?s).
      decode ?final_type ∧ ?final_type = ?f ∧ ?s = ?x ∧ decode ?x
    ∃(?s = int) ?final_type (?f = ?s -> ?s).
      decode ?final_type ∧ ?final_type = ?f ∧ decode ?s
    ∃(?s = int) (?final_type = ?s -> ?s). decode ?final_type ∧ decode ?s
  
  Inferred type:
    int -> int
  
  Elaborated term:
    lambda (x : int). (x : int)
  

## An erroneous program

  $ minihell $FLAGS error.test
  Input term:
    (lambda x. (x : int)) (lambda y. y)
  
  Generated constraint:
    ∃?final_type.
      (∃?x (?f = ?x -> ?final_type).
        (∃?x/1 ?s (?f/1 = ?x/1 -> ?s).
          (∃(?a = int). decode ?x/1 ∧ ?x/1 = ?a ∧ ?s = ?a) ∧ ?f = ?f/1)
        ∧ (∃?x/2 ?s/1 (?f/2 = ?x/2 -> ?s/1).
          decode ?x/2 ∧ ?x/2 = ?s/1 ∧ ?x = ?f/2))
      ∧ decode ?final_type
  
  Error:
      int
    incompatible with
      α -> α
  

## Examples with products

  $ minihell $FLAGS curry.test
  Input term:
    lambda f. lambda x. lambda y. f (x, y)
  
  Generated constraint:
    ∃?final_type.
      (∃?x ?s (?f = ?x -> ?s).
        (∃?x/1 ?s/1 (?f/1 = ?x/1 -> ?s/1).
          (∃?x/2 ?s/2 (?f/2 = ?x/2 -> ?s/2).
            (∃?x/3 (?f/3 = ?x/3 -> ?s/2).
              decode ?x
              ∧ ?x = ?f/3
              ∧ (∃?a ?b (?w = {?a * ?b}).
                decode ?x/1 ∧ ?x/1 = ?a ∧ decode ?x/2 ∧ ?x/2 = ?b ∧ ?x/3 = ?w))
            ∧ ?s/1 = ?f/2)
          ∧ ?s = ?f/1)
        ∧ ?final_type = ?f)
      ∧ decode ?final_type
  
  Inferred type:
    ({γ * β} -> α) -> γ -> β -> α
  
  Elaborated term:
    lambda (f : {γ * β} -> α). lambda (x : γ). lambda (y : β). f (x, y)
  

  $ minihell $FLAGS uncurry.test
  Input term:
    lambda f. lambda p. let (x, y) = p in f x y
  
  Generated constraint:
    ∃?final_type.
      (∃?x ?s (?f = ?x -> ?s).
        (∃?x/1 ?s/1 (?f/1 = ?x/1 -> ?s/1).
          (∃?x1 ?x2 (?pair = {?x1 * ?x2}).
            decode ?x/1
            ∧ ?x/1 = ?pair
            ∧ (∃?x/2 (?f/2 = ?x/2 -> ?s/1).
              (∃?x/3 (?f/3 = ?x/3 -> ?f/2).
                decode ?x ∧ ?x = ?f/3 ∧ decode ?x1 ∧ ?x1 = ?x/3)
              ∧ decode ?x2
              ∧ ?x2 = ?x/2)
            ∧ decode ?x1
            ∧ decode ?x2)
          ∧ ?s = ?f/1)
        ∧ ?final_type = ?f)
      ∧ decode ?final_type
  
  Inferred type:
    (β -> γ -> α) -> {β * γ} -> α
  
  Elaborated term:
    lambda
    (f : β -> γ -> α).
      lambda (p : {β * γ}). let ((x : β), (y : γ)) = p in f x y
  
## Cyclic types

Unification can sometimes create cyclic types. We decide to reject
these situations with an error. (We could also accept those as they
preserve type-safety, but they have the issue, just like the
OCaml -rectypes option, that they allow to write somewhat-nonsensical
program, and our random term generator will be very good at finding
a lot of those.)

  $ minihell $FLAGS --log-solver selfapp.test
  Input term:
    lambda x. x x
  
  Generated constraint:
    ∃?final_type.
      (∃?x ?s (?f = ?x -> ?s).
        (∃?x/1 (?f/1 = ?x/1 -> ?s).
          decode ?x ∧ ?x = ?f/1 ∧ decode ?x ∧ ?x = ?x/1)
        ∧ ?final_type = ?f)
      ∧ decode ?final_type
  
  Constraint solving log:
    ∃?final_type.
      decode ?final_type
      ∧ (∃?x ?s (?f = ?x -> ?s).
        ?final_type = ?f
        ∧ (∃?x/1 (?f/1 = ?x/1 -> ?s).
          ?x = ?x/1 ∧ decode ?x ∧ ?x = ?f/1 ∧ decode ?x))
    ∃?final_type.
      decode ?final_type
      ∧ (∃?x ?s (?f = ?x -> ?s).
        ?final_type = ?f
        ∧ (∃?x/1 (?f/1 = ?x/1 -> ?s).
          ?x = ?x/1 ∧ decode ?x ∧ ?x = ?f/1 ∧ decode ?x))
    ∃?x ?final_type.
      decode ?final_type
      ∧ (∃?s (?f = ?x -> ?s).
        ?final_type = ?f
        ∧ (∃?x/1 (?f/1 = ?x/1 -> ?s).
          ?x = ?x/1 ∧ decode ?x ∧ ?x = ?f/1 ∧ decode ?x))
    ∃?x ?s ?final_type.
      decode ?final_type
      ∧ (∃(?f = ?x -> ?s).
        ?final_type = ?f
        ∧ (∃?x/1 (?f/1 = ?x/1 -> ?s).
          ?x = ?x/1 ∧ decode ?x ∧ ?x = ?f/1 ∧ decode ?x))
    ∃?x ?s ?final_type (?f = ?x -> ?s).
      decode ?final_type
      ∧ ?final_type = ?f
      ∧ (∃?x/1 (?f/1 = ?x/1 -> ?s).
        ?x = ?x/1 ∧ decode ?x ∧ ?x = ?f/1 ∧ decode ?x)
    ∃?x/1 ?x ?s ?final_type (?f = ?x -> ?s).
      decode ?final_type
      ∧ ?final_type = ?f
      ∧ (∃(?f/1 = ?x/1 -> ?s). ?x = ?x/1 ∧ decode ?x ∧ ?x = ?f/1 ∧ decode ?x)
    ∃?x/1 ?x ?s ?final_type (?f/1 = ?x/1 -> ?s) (?f = ?x -> ?s).
      decode ?final_type
      ∧ ?final_type = ?f
      ∧ ?x = ?x/1
      ∧ decode ?x
      ∧ ?x = ?f/1
      ∧ decode ?x
    ∃?x/1 ?s (?x = ?x/1 -> ?s) ?s ?final_type (?f = ?x -> ?s).
      decode ?final_type ∧ ?final_type = ?f ∧ ⊥ ∧ decode ?x ∧ decode ?x
    ∃?x/1 ?s (?x = ?x/1 -> ?s) ?s (?final_type = ?x -> ?s).
      decode ?final_type ∧ ⊥ ∧ decode ?x ∧ decode ?x
  
  Error:
    cycle on constraint variable
    ?x
  
## Generator tests

This gives example outputs for my implementation. It is completely
fine if your own implementation produces different (sensible) results.

There are not many programs with depth 3, 4 and 5.

  $ minigen --exhaustive --depth 5 --count 50
  lambda
  (x : α). lambda (x/1 : β). lambda (x/2 : γ). lambda (x/3 : δ). x/3
  
  lambda
  (x : α). lambda (x/1 : β). lambda (x/2 : γ). lambda (x/3 : δ). x/2
  
  lambda
  (x : α). lambda (x/1 : β). lambda (x/2 : γ). lambda (x/3 : δ). x/1
  
  lambda
  (x : α). lambda (x/1 : β). lambda (x/2 : γ). lambda (x/3 : δ). x
  
  lambda (x : α). lambda (x/1 : γ -> β). lambda (x/2 : γ). x/1 x/2
  
  lambda (x : α -> δ). lambda (x/1 : β). lambda (x/2 : α). x x/2
  
  lambda (x : α). lambda (x/1 : β). lambda (x/2 : β -> γ). x/2 x/1
  
  lambda (x : α -> δ). lambda (x/1 : α). lambda (x/2 : β). x x/1
  
  lambda (x : α). lambda (x/1 : β). lambda (x/2 : α -> γ). x/2 x
  
  lambda (x : δ). lambda (x/1 : δ -> α). lambda (x/2 : β). x/1 x
  
  lambda (x : γ). lambda (x/1 : δ). lambda (x/2 : α). (x/2, x/2)
  
  lambda (x : β). lambda (x/1 : γ). lambda (x/2 : δ). (x/1, x/2)
  
  lambda (x : α). lambda (x/1 : β). lambda (x/2 : γ). (x, x/2)
  
  lambda (x : δ). lambda (x/1 : α). lambda (x/2 : β). (x/2, x/1)
  
  lambda (x : γ). lambda (x/1 : δ). lambda (x/2 : α). (x/1, x/1)
  
  lambda (x : β). lambda (x/1 : γ). lambda (x/2 : δ). (x, x/1)
  
  lambda (x : α). lambda (x/1 : β). lambda (x/2 : γ). (x/2, x)
  
  lambda (x : δ). lambda (x/1 : α). lambda (x/2 : β). (x/1, x)
  
  lambda (x : γ). lambda (x/1 : δ). lambda (x/2 : α). (x, x)
  
  lambda (x : (β -> γ) -> β). lambda (x/1 : β -> γ). x/1 (x x/1)
  
  lambda (x : β). lambda (x/1 : β -> β). x/1 (x/1 x)
  
  lambda (x : γ -> γ). lambda (x/1 : γ). x (x x/1)
  
  lambda (x : α -> δ). lambda (x/1 : (α -> δ) -> α). x (x/1 x)
  
  lambda (x : β -> β -> α). lambda (x/1 : β). x x/1 x/1
  
  lambda (x : δ). lambda (x/1 : δ -> δ -> α). x/1 x x
  
  lambda (x : α). lambda (x/1 : {α * α} -> β). x/1 (x, x)
  
  lambda (x : {δ * δ} -> γ). lambda (x/1 : δ). x (x/1, x/1)
  
  lambda (x : β -> α). lambda (x/1 : β). (x/1, x x/1)
  
  lambda (x : α). lambda (x/1 : α -> β). (x/1, x/1 x)
  
  lambda (x : β -> α). lambda (x/1 : β). (x, x x/1)
  
  lambda (x : α). lambda (x/1 : α -> β). (x, x/1 x)
  
  lambda (x : β -> α). lambda (x/1 : β). (x x/1, x/1)
  
  lambda (x : α). lambda (x/1 : α -> β). (x/1 x, x/1)
  
  lambda (x : γ). lambda (x/1 : δ). ((x/1, x/1), x/1)
  
  lambda (x : α). lambda (x/1 : β). ((x, x/1), x/1)
  
  lambda (x : γ). lambda (x/1 : δ). ((x/1, x), x/1)
  
  lambda (x : α). lambda (x/1 : β). ((x, x), x/1)
  
  lambda (x : δ -> γ). lambda (x/1 : δ). (x x/1, x)
  
  lambda (x : γ). lambda (x/1 : γ -> δ). (x/1 x, x)
  
  lambda (x : α). lambda (x/1 : β). ((x/1, x/1), x)
  
  lambda (x : γ). lambda (x/1 : δ). ((x, x/1), x)
  
  lambda (x : α). lambda (x/1 : β). ((x/1, x), x)
  
  lambda (x : γ). lambda (x/1 : δ). ((x, x), x)
  
  lambda (x : γ). lambda (x/1 : δ). (x/1, (x/1, x/1))
  
  lambda (x : α). lambda (x/1 : β). (x/1, (x, x/1))
  
  lambda (x : γ). lambda (x/1 : δ). (x/1, (x/1, x))
  
  lambda (x : α). lambda (x/1 : β). (x/1, (x, x))
  
  lambda (x : γ). lambda (x/1 : δ). (x, (x/1, x/1))
  
  lambda (x : α). lambda (x/1 : β). (x, (x, x/1))
  
  lambda (x : γ). lambda (x/1 : δ). (x, (x/1, x))

  $ minigen --exhaustive --depth 7 --count 50
  lambda
  (x : α).
    lambda
    (x/1 : β).
      lambda
      (x/2 : γ).
        lambda (x/3 : δ). lambda (x/4 : α/1). lambda (x/5 : β/1). x/5
  
  lambda
  (x : γ).
    lambda
    (x/1 : δ).
      lambda
      (x/2 : α).
        lambda (x/3 : β). lambda (x/4 : γ/1). lambda (x/5 : δ/1). x/4
  
  lambda
  (x : α).
    lambda
    (x/1 : β).
      lambda
      (x/2 : γ).
        lambda (x/3 : δ). lambda (x/4 : α/1). lambda (x/5 : β/1). x/3
  
  lambda
  (x : γ).
    lambda
    (x/1 : δ).
      lambda
      (x/2 : α).
        lambda (x/3 : β). lambda (x/4 : γ/1). lambda (x/5 : δ/1). x/2
  
  lambda
  (x : α).
    lambda
    (x/1 : β).
      lambda
      (x/2 : γ).
        lambda (x/3 : δ). lambda (x/4 : α/1). lambda (x/5 : β/1). x/1
  
  lambda
  (x : γ).
    lambda
    (x/1 : δ).
      lambda
      (x/2 : α).
        lambda (x/3 : β). lambda (x/4 : γ/1). lambda (x/5 : δ/1). x
  
  lambda
  (x : α).
    lambda
    (x/1 : β).
      lambda
      (x/2 : γ). lambda (x/3 : α/1 -> δ). lambda (x/4 : α/1). x/3 x/4
  
  lambda
  (x : β).
    lambda
    (x/1 : γ).
      lambda (x/2 : α -> δ). lambda (x/3 : β/1). lambda (x/4 : α). x/2 x/4
  
  lambda
  (x : γ).
    lambda
    (x/1 : α -> δ).
      lambda (x/2 : β). lambda (x/3 : γ/1). lambda (x/4 : α). x/1 x/4
  
  lambda
  (x : α -> δ).
    lambda
    (x/1 : β).
      lambda (x/2 : γ). lambda (x/3 : δ/1). lambda (x/4 : α). x x/4
  
  lambda
  (x : γ).
    lambda
    (x/1 : δ).
      lambda (x/2 : α). lambda (x/3 : β). lambda (x/4 : β -> γ/1). x/4 x/3
  
  lambda
  (x : δ).
    lambda
    (x/1 : α).
      lambda (x/2 : γ -> β). lambda (x/3 : γ). lambda (x/4 : δ/1). x/2 x/3
  
  lambda
  (x : α).
    lambda
    (x/1 : γ -> β).
      lambda (x/2 : δ). lambda (x/3 : γ). lambda (x/4 : α/1). x/1 x/3
  
  lambda
  (x : γ -> β).
    lambda
    (x/1 : δ).
      lambda (x/2 : α). lambda (x/3 : γ). lambda (x/4 : β/1). x x/3
  
  lambda
  (x : α).
    lambda
    (x/1 : β).
      lambda (x/2 : γ). lambda (x/3 : δ). lambda (x/4 : γ -> α/1). x/4 x/2
  
  lambda
  (x : β).
    lambda
    (x/1 : γ).
      lambda (x/2 : δ). lambda (x/3 : δ -> α). lambda (x/4 : β/1). x/3 x/2
  
  lambda
  (x : γ).
    lambda
    (x/1 : α -> δ).
      lambda (x/2 : α). lambda (x/3 : β). lambda (x/4 : γ/1). x/1 x/2
  
  lambda
  (x : α -> δ).
    lambda
    (x/1 : β).
      lambda (x/2 : α). lambda (x/3 : γ). lambda (x/4 : δ/1). x x/2
  
  lambda
  (x : γ).
    lambda
    (x/1 : δ).
      lambda (x/2 : α). lambda (x/3 : β). lambda (x/4 : δ -> γ/1). x/4 x/1
  
  lambda
  (x : δ).
    lambda
    (x/1 : α).
      lambda (x/2 : β). lambda (x/3 : α -> γ). lambda (x/4 : δ/1). x/3 x/1
  
  lambda
  (x : α).
    lambda
    (x/1 : β).
      lambda (x/2 : β -> γ). lambda (x/3 : δ). lambda (x/4 : α/1). x/2 x/1
  
  lambda
  (x : γ -> β).
    lambda
    (x/1 : γ).
      lambda (x/2 : δ). lambda (x/3 : α). lambda (x/4 : β/1). x x/1
  
  lambda
  (x : α).
    lambda
    (x/1 : β).
      lambda (x/2 : γ). lambda (x/3 : δ). lambda (x/4 : α -> α/1). x/4 x
  
  lambda
  (x : β).
    lambda
    (x/1 : γ).
      lambda (x/2 : δ). lambda (x/3 : β -> α). lambda (x/4 : β/1). x/3 x
  
  lambda
  (x : γ).
    lambda
    (x/1 : δ).
      lambda (x/2 : γ -> α). lambda (x/3 : β). lambda (x/4 : γ/1). x/2 x
  
  lambda
  (x : δ).
    lambda
    (x/1 : δ -> α).
      lambda (x/2 : β). lambda (x/3 : γ). lambda (x/4 : δ/1). x/1 x
  
  lambda
  (x : α).
    lambda
    (x/1 : β).
      lambda (x/2 : γ). lambda (x/3 : δ). lambda (x/4 : α/1). (x/4, x/4)
  
  lambda
  (x : β).
    lambda
    (x/1 : γ).
      lambda (x/2 : δ). lambda (x/3 : α). lambda (x/4 : β/1). (x/3, x/4)
  
  lambda
  (x : γ).
    lambda
    (x/1 : δ).
      lambda (x/2 : α). lambda (x/3 : β). lambda (x/4 : γ/1). (x/2, x/4)
  
  lambda
  (x : δ).
    lambda
    (x/1 : α).
      lambda (x/2 : β). lambda (x/3 : γ). lambda (x/4 : δ/1). (x/1, x/4)
  
  lambda
  (x : α).
    lambda
    (x/1 : β).
      lambda (x/2 : γ). lambda (x/3 : δ). lambda (x/4 : α/1). (x, x/4)
  
  lambda
  (x : β).
    lambda
    (x/1 : γ).
      lambda (x/2 : δ). lambda (x/3 : α). lambda (x/4 : β/1). (x/4, x/3)
  
  lambda
  (x : γ).
    lambda
    (x/1 : δ).
      lambda (x/2 : α). lambda (x/3 : β). lambda (x/4 : γ/1). (x/3, x/3)
  
  lambda
  (x : δ).
    lambda
    (x/1 : α).
      lambda (x/2 : β). lambda (x/3 : γ). lambda (x/4 : δ/1). (x/2, x/3)
  
  lambda
  (x : α).
    lambda
    (x/1 : β).
      lambda (x/2 : γ). lambda (x/3 : δ). lambda (x/4 : α/1). (x/1, x/3)
  
  lambda
  (x : β).
    lambda
    (x/1 : γ).
      lambda (x/2 : δ). lambda (x/3 : α). lambda (x/4 : β/1). (x, x/3)
  
  lambda
  (x : γ).
    lambda
    (x/1 : δ).
      lambda (x/2 : α). lambda (x/3 : β). lambda (x/4 : γ/1). (x/4, x/2)
  
  lambda
  (x : δ).
    lambda
    (x/1 : α).
      lambda (x/2 : β). lambda (x/3 : γ). lambda (x/4 : δ/1). (x/3, x/2)
  
  lambda
  (x : α).
    lambda
    (x/1 : β).
      lambda (x/2 : γ). lambda (x/3 : δ). lambda (x/4 : α/1). (x/2, x/2)
  
  lambda
  (x : β).
    lambda
    (x/1 : γ).
      lambda (x/2 : δ). lambda (x/3 : α). lambda (x/4 : β/1). (x/1, x/2)
  
  lambda
  (x : γ).
    lambda
    (x/1 : δ).
      lambda (x/2 : α). lambda (x/3 : β). lambda (x/4 : γ/1). (x, x/2)
  
  lambda
  (x : δ).
    lambda
    (x/1 : α).
      lambda (x/2 : β). lambda (x/3 : γ). lambda (x/4 : δ/1). (x/4, x/1)
  
  lambda
  (x : α).
    lambda
    (x/1 : β).
      lambda (x/2 : γ). lambda (x/3 : δ). lambda (x/4 : α/1). (x/3, x/1)
  
  lambda
  (x : β).
    lambda
    (x/1 : γ).
      lambda (x/2 : δ). lambda (x/3 : α). lambda (x/4 : β/1). (x/2, x/1)
  
  lambda
  (x : γ).
    lambda
    (x/1 : δ).
      lambda (x/2 : α). lambda (x/3 : β). lambda (x/4 : γ/1). (x/1, x/1)
  
  lambda
  (x : δ).
    lambda
    (x/1 : α).
      lambda (x/2 : β). lambda (x/3 : γ). lambda (x/4 : δ/1). (x, x/1)
  
  lambda
  (x : α).
    lambda
    (x/1 : β).
      lambda (x/2 : γ). lambda (x/3 : δ). lambda (x/4 : α/1). (x/4, x)
  
  lambda
  (x : β).
    lambda
    (x/1 : γ).
      lambda (x/2 : δ). lambda (x/3 : α). lambda (x/4 : β/1). (x/3, x)
  
  lambda
  (x : γ).
    lambda
    (x/1 : δ).
      lambda (x/2 : α). lambda (x/3 : β). lambda (x/4 : γ/1). (x/2, x)
  
  lambda
  (x : δ).
    lambda
    (x/1 : α).
      lambda (x/2 : β). lambda (x/3 : γ). lambda (x/4 : δ/1). (x/1, x)

An example of random sampling output at higher depth.

  $ minigen --seed 4 --depth 9 --count 10
  lambda
  (x : α).
    lambda
    (x/17 : δ -> δ -> γ -> β).
      lambda
      (x/18 : γ).
        lambda (x/19 : δ). lambda (x/1a : α/1). x/17 x/19 x/19 x/18
  
  lambda
  (x : γ -> β).
    lambda
    (x/17 : γ).
      lambda
      (x/18 : δ).
        lambda
        (x/19 : α).
          let (x/24 : {{δ * γ} * δ}) = ((x/18, x/17), x/18) in x x/17
  
  lambda
  (x : γ -> β).
    lambda
    (x/17 : γ).
      lambda
      (x/18 : δ).
        lambda
        (x/19 : α).
          let (x/24 : {{δ * γ} * δ}) = ((x/18, x/17), x/18) in x x/17
  
  lambda
  (x : α).
    lambda
    (x/17 : β).
      lambda
      (x/18 : γ).
        lambda
        (x/19 : {β * α/1} -> δ).
          lambda (x/1a : α -> α/1). x/19 (x/17, x/1a x)
  
  lambda
  (x : β).
    lambda
    (x/17 : γ).
      lambda (x/18 : β -> δ). ((((x, x/18 x), x/18), x/17), x/17)
  
  lambda
  (x : α).
    lambda
    (x/17 : β).
      lambda (x/18 : γ). (((((x/17, x/17), x/17), x/18), x/17), x/17)
  
  lambda
  (x : α).
    lambda
    (x/17 : β).
      lambda
      (x/18 : γ).
        lambda
        (x/19 : α/1 -> δ).
          lambda (x/1a : γ -> α -> α/1). x/19 (x/1a x/18 x)
  
  lambda
  (x : β).
    lambda
    (x/17 : γ).
      lambda
      (x/18 : δ).
        lambda
        (x/19 : δ -> γ -> α).
          let (x/24 : {δ * γ}) = (x/18, x/17) in x/19 x/18 x/17
  
  lambda
  (x : δ).
    lambda
    (x/17 : {α * β}).
      lambda
      (x/18 : β).
        lambda
        (x/19 : {α * β} -> α). lambda (x/1a : γ). x/19 (x/19 x/17, x/18)
  
  lambda
  (x : α -> δ).
    lambda
    (x/17 : α). lambda (x/18 : β). ((((x, x x/17), x/18), x/17), x/17)
